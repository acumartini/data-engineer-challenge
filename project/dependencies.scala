import sbt._

object Dependencies {

	object Version {
		val akka = "2.4.4"
	}

	lazy val frontend = common ++ tests
	lazy val backend = common ++ tests

	val common = Seq(
		"com.typesafe.akka" %% "akka-actor" % Version.akka,
		"com.typesafe.akka" %% "akka-cluster" % Version.akka,
		"com.typesafe.akka" %% "akka-cluster-metrics" % Version.akka,
		"com.typesafe.akka" %% "akka-cluster-tools" % Version.akka,
		"com.typesafe.akka" %% "akka-slf4j" % Version.akka,
		"com.google.guava" % "guava" % "19.0",
		"org.scalaz" %% "scalaz-core" % "7.2.2"
	)

	val tests = Seq(
		"org.scalatest" %% "scalatest" % "2.2.6" % "test",
		"org.scalatestplus.play" %% "scalatestplus-play" % "1.5.0" % "test",
		"com.typesafe.akka" %% "akka-testkit" % Version.akka % "test",
		"org.mockito" % "mockito-core" % "1.10.19",
		"org.scalaj" %% "scalaj-http" % "2.3.0"
	)

}
