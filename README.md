# Data Engineer Programming Challenge

## Goal: Part 1
   
Building a scalable play application to accept, aggregate, and persist incoming click data.

* Play Application
* There will be at most 100 events per request body. The application should scale to as many events/s as possible, but will need to process at least 400/s. There will be at most 10,000 unique itemIds.
* The application should respond with 200 OK.

Note: The aggregation data is reduced and persisted to file system in the click_data directory of the backend module.  If the backend nodes are deployed on separate machines the file will only be persisted to the file system of the machine with active AggregationPersisterActor cluster singleton.  Ideally this storage location would centralized/independent from the backend deployment configuration.

TODO

* Improve persistence performance by reducing aggregates locally on each backend node and sending a single message from each node to the cluster singleton persister for final reduction and writing to the file system.
* Investigate missing OK responses when running the IntegrationTest

## Goal: Part 2
   
R data conversion

## Launching the Application

Run each line in a new terminal from activator.

```
backend/run 2551
backend/run 2552
frontend/run
```

Any number of backend instances can be created (backend/run) after the two seed nodes are started and they will auto connect to the cluster to provide more aggregation processors.  Likewise, any number of frontend nodes can be created (frontend/run \<port\>) to increase throughput given that a HTTP server (i.e. nginx) is in place to distribute requests.

## Time Sheet

####  Part 1:

|DATE	    |START	 	|END        |HOURS  |
|-----------|-----------|-----------|-------|
|5/6		|4:15pm	 	|5:45pm     |1.5    |
|5/7		|10:00am	|1:45pm     |3.75   |
|5/7		|2:15pm	 	|3:30pm     |1.25   |
|5/7		|4:15pm	 	|9:15pm     |5.0    |
|5/8		|8:00am	 	|1:30pm     |5.5    |
|5/8		|1:30pm	 	|3:15pm     |1.75   |
|5/8		|4:45pm		|8:00pm     |3.25   |
|5/8		|8:30pm		|9:15pm     |0.75   |

##### Total Hours Part 1 - 22.75