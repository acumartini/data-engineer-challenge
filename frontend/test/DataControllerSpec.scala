import ControllerSpec.EmptyActor
import akka.testkit.TestActorRef
import controllers.DataController
import messages.Common.{AggregationComplete, DataPoints}
import model.ClickData
import play.api.libs.json.Json
import play.api.mvc.Result
import play.api.test.FakeRequest
import play.api.test.Helpers._

import scala.concurrent.Future

/**
	*
	* @author Adam Martini
	*/
class DataControllerSpec extends AkkaControllerSpec {

	"DataController#data" should {
		"should return Status OK" in {
			val jsonData = Json.parse(
				//language="json"
				"""[{ "timestamp": 100, "itemId": 2 }]"""
			)
			val expectedData = DataPoints(List(ClickData(100, 2)))

			val mockDataService = TestActorRef(new EmptyActor() {
				override def receive = {
					case data: DataPoints =>
						data shouldEqual expectedData
						sender() ! AggregationComplete
				}
			})

			val controller = new DataController(system, mockDataService)(scala.concurrent.ExecutionContext.Implicits.global)
			val result: Future[Result] = controller.handleIncomingData().apply(
				FakeRequest(POST, "/data").withBody(jsonData)
			)

			status(result) shouldEqual OK
		}
		"should return Status BAD_REQUEST with invalid data" in {
			val invalidJsonData = Json.parse(
				//language="json"
				"""{ "timestamp": 100, "itemId": 2 }"""
			)
			val mockDataService = TestActorRef(new EmptyActor())

			val controller = new DataController(system, mockDataService)(scala.concurrent.ExecutionContext.Implicits.global)
			val result: Future[Result] = controller.handleIncomingData().apply(
				FakeRequest(POST, "/data").withBody(invalidJsonData)
			)

			status(result) shouldEqual BAD_REQUEST
		}
	}

}