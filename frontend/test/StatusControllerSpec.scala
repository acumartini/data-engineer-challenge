import controllers.StatusController
import org.scalatestplus.play.PlaySpec
import play.api.mvc.{Result, Results}
import play.api.test.FakeRequest
import play.api.test.Helpers._

import scala.concurrent.Future

/**
	*
	* @author Adam Martini
	*/
class StatusControllerSpec extends PlaySpec with Results {

	"StatusController#index" should {
		"should be valid" in {
			val controller = new StatusController()
			val result: Future[Result] = controller.index().apply(FakeRequest())
			val bodyText: String = contentAsString(result)
			bodyText mustBe "Deployed Successfully"
		}
	}

}