import akka.actor.{Actor, ActorSystem}
import akka.testkit.{ImplicitSender, TestKit}
import com.typesafe.config.ConfigFactory
import org.scalatest.concurrent.Eventually
import org.scalatest.mock.MockitoSugar
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import play.api.http.Status

/**
	*
	* @author Adam Martini
	*/
abstract class AkkaControllerSpec extends TestKit(ActorSystem("ControllerSpec",
	ConfigFactory.parseString(ControllerSpec.config)))
	with ImplicitSender
	with WordSpecLike
	with Matchers
	with BeforeAndAfterAll
	with Eventually
	with MockitoSugar
	with Status

object ControllerSpec {
	val config =
		"""
			|akka {
			|    loglevel = "WARNING"
			|
			|    actor {
			|        provider = "akka.cluster.ClusterActorRefProvider"
			|    }
			|
			|    remote {
			|        log-remote-lifecycle-events = off
			|    netty.tcp {
			|        hostname = "127.0.0.1"
			|        port = 0
			|    }
			|  }
			|}
		""".stripMargin

	class EmptyActor extends Actor {
		def receive = {
			case msg =>
		}
	}
}