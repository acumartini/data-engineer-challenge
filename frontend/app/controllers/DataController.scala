package controllers

import javax.inject._

import akka.actor.{ActorRef, ActorSystem}
import akka.pattern.ask
import akka.util.Timeout
import messages.Common.{AggregationComplete, DataPoints}
import model.ClickData
import play.api.libs.json.{JsError, Json}
import play.api.mvc._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

/**
	* Receives incoming click data and sends it to the backend for processing.
	*/
@Singleton
class DataController @Inject()(
	system: ActorSystem,
	@Named("dataService") dataService: ActorRef
)(implicit exec: ExecutionContext) extends Controller {

	implicit val timeout = Timeout(10, SECONDS)

	/**
		* Asynchronously sends click data to backend aggregators.
		*/
	def handleIncomingData = Action.async(BodyParsers.parse.json) { request =>
		request.body.validate[List[ClickData]].fold(
			errors => Future(
				BadRequest(Json.obj("message" -> JsError.toJson(errors)))
			),
			clickData => {
				(dataService ? DataPoints(clickData)) map {
					case AggregationComplete => Ok("Success")
					case _ => InternalServerError
				}
			}
		)
	}

}
