package controllers

import javax.inject._

import play.api.mvc._

/**
 * Provides confirmation of successful deployment.
 */
@Singleton
class StatusController @Inject() extends Controller {

  def index = Action {
    Ok(views.html.index())
  }

}
