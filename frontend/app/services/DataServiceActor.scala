package services

import javax.inject.Inject

import akka.actor._
import akka.routing.FromConfig
import messages.Common.DataPoints

/**
	* The connection point between frontend and  cluster
	*/
class DataServiceActor @Inject() extends Actor with ActorLogging {

	val backend = context.actorOf(FromConfig.props(), name = "dataAggregationRouter")

	def receive = {
		case data: DataPoints => backend forward data
	}

}