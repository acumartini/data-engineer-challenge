package model

import play.api.libs.json.{Json, Writes, JsPath, Reads}
import play.api.libs.functional.syntax._

/**
	*
	* @author Adam Martini
	*/
case class ClickData(
	timestamp: Long,
	itemId: Int
)
object ClickData {
	implicit val clickDataReads: Reads[ClickData] = (
		(JsPath \ "timestamp").read[Long] and
		(JsPath \ "itemId").read[Int]
	)(ClickData.apply _)
	implicit val clickDataWrites = new Writes[ClickData] {
		def writes(clickData: ClickData) = Json.obj(
			"timestamp" -> clickData.timestamp,
			"itemId" -> clickData.itemId
		)
	}
}
