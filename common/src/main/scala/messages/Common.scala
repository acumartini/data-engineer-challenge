package messages

import model.ClickData

object Common {

	type ClickCountMap = Map[Int, Long]

	// Frontend <> Backend
	case class DataPoints(data: List[ClickData])
	case object AggregationComplete

	// Aggregation
	case object AggregatorRegistration
	case object SlidingWindowTick

	// Persistence
	case object PersistenceTick

	// Aggregator <> Persistor
	case object AggregationDataRequest
	case class AggregationData(data: ClickCountMap)

}