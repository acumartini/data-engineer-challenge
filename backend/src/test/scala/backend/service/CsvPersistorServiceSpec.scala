package backend.service

import org.scalatest.{FlatSpec, Matchers}

/**
	*
	* @author Adam Martini
	*/

class CsvPersistorServiceSpec extends FlatSpec with Matchers {

	s"A ${classOf[CsvPersistorService].getSimpleName}" should "properly transform click data to csv" in {
		val service: CsvPersistorService = CsvPersistorService

		val data = Map(1 -> 4l, 3 -> 2l, 4 -> 2l)
		val expectedCsvStr =
			"""1,4
				|3,2
				|4,2""".stripMargin

		service.transform(data).toString shouldEqual expectedCsvStr
	}

}
