package backend.aggregtion

import backend.aggregation.DataAggregationActor
import messages.Common._
import model.ClickData

import scala.concurrent.duration._

/**
	*
	* @author Adam Martini
	*/
class DataAggregationActorSpec extends AbstractAggregationActorSpec {

	s"A ${classOf[DataAggregationActor].getSimpleName}" should {
		"Receive and aggregate click data" in {
			val actorRef = system.actorOf(DataAggregationActor.props())
			val dataPoints = DataPoints(
				List(ClickData(100, 1), ClickData(200, 1), ClickData(300, 3), ClickData(400, 4))
			)
			val expectedData = Map(1 -> 2, 3 -> 1, 4 -> 1)
			var reducedData: ClickCountMap = Map.empty

			within(500.millis) {
				actorRef ! dataPoints
				expectMsg(AggregationComplete)

				actorRef ! AggregationDataRequest
				receiveWhile(500.millis) {
					case AggregationData(data) => reducedData = data
				}
			}
			reducedData shouldEqual expectedData
		}
		"Properly increment sliding window and clear aggregation data" in {
			val actorRef = system.actorOf(DataAggregationActor.props(2))
			val dataPoints = DataPoints(
				List(ClickData(100, 1), ClickData(200, 1), ClickData(300, 3), ClickData(400, 4))
			)
			val expectedData = Map(1 -> 4, 3 -> 2, 4 -> 2)
			var reducedData: ClickCountMap = Map.empty

			within(500.millis) {
				actorRef ! dataPoints
				expectMsg(AggregationComplete)
				actorRef ! SlidingWindowTick
				actorRef ! dataPoints
				expectMsg(AggregationComplete)

				actorRef ! AggregationDataRequest
				receiveWhile(500.millis) {
					case AggregationData(data) => reducedData = data
				}
			}
			reducedData shouldEqual expectedData

			within(500.millis) {
				actorRef ! SlidingWindowTick
				actorRef ! dataPoints
				expectMsg(AggregationComplete)

				actorRef ! AggregationDataRequest
				receiveWhile(500.millis) {
					case AggregationData(data) => reducedData = data
				}
			}
			reducedData shouldEqual expectedData
		}
	}

}
