package backend.aggregtion

import akka.actor.{Actor, ActorSystem}
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit}
import com.typesafe.config.ConfigFactory
import org.scalatest.concurrent.Eventually
import org.scalatest.mock.MockitoSugar
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

/**
	*
	* @author Adam Martini
	*/
abstract class AbstractAggregationActorSpec extends TestKit(ActorSystem("AggregationActorSpec",
	ConfigFactory.parseString(AggregationActorSpec.config)))
	with DefaultTimeout
	with ImplicitSender
	with WordSpecLike
	with Matchers
	with BeforeAndAfterAll
	with Eventually
	with MockitoSugar

object AggregationActorSpec {
	val config =
		"""
			|akka {
			|    loglevel = "WARNING"
			|
			|    actor {
			|        provider = "akka.cluster.ClusterActorRefProvider"
			|    }
			|
			|    remote {
			|        log-remote-lifecycle-events = off
			|    netty.tcp {
			|        hostname = "127.0.0.1"
			|        port = 0
			|    }
			|  }
			|}
		""".stripMargin

	class EmptyActor extends Actor {
		def receive = {
			case msg =>
		}
	}
}