package backend.aggregtion

import akka.actor.{PoisonPill, Props}
import akka.testkit.TestActorRef
import backend.aggregation.{AggregationPersistorActor, DataAggregationActor}
import backend.aggregtion.AggregationActorSpec.EmptyActor
import backend.service.PersistorService
import messages.Common._
import model.ClickData
import org.mockito.Mockito._
import org.scalatest.time.{Seconds, Span}

import scala.concurrent.duration._

/**
	*
	* @author Adam Martini
	*/
class AggregationPersistorActorSpec extends AbstractAggregationActorSpec {

	override implicit val patienceConfig =
		PatienceConfig(timeout = scaled(Span(20, Seconds)), interval = scaled(Span(5, Seconds)))

	val mockPersistorService = mock[PersistorService]

	s"A ${classOf[AggregationPersistorActor].getSimpleName}" should {
		"Properly maintain a list of available aggregators" in {
			val actorRef = TestActorRef(new AggregationPersistorActor(mockPersistorService))

			val emptyActor1 = system.actorOf(Props[EmptyActor])
			val emptyActor2 = system.actorOf(Props[EmptyActor])

			actorRef.tell(AggregatorRegistration, emptyActor1)
			actorRef.tell(AggregatorRegistration, emptyActor1)
			actorRef.tell(AggregatorRegistration, emptyActor2)

			actorRef.underlyingActor.aggregators.size shouldEqual 2
			actorRef.underlyingActor.aggregators should contain (emptyActor1)
			actorRef.underlyingActor.aggregators should contain (emptyActor2)

			emptyActor2 ! PoisonPill

			eventually {
				actorRef.underlyingActor.aggregators.size shouldEqual 1
				actorRef.underlyingActor.aggregators should contain (emptyActor1)
			}
		}

		"Properly accumulate and persist aggregation data provided by available aggregators" in {
			val actorRef = TestActorRef(new AggregationPersistorActor(mockPersistorService))

			val aggActor1 = system.actorOf(DataAggregationActor.props())
			val aggActor2 = system.actorOf(DataAggregationActor.props())
			val aggActor3 = system.actorOf(DataAggregationActor.props())

			actorRef.tell(AggregatorRegistration, aggActor1)
			actorRef.tell(AggregatorRegistration, aggActor2)
			actorRef.tell(AggregatorRegistration, aggActor3)

			val dataPoints = DataPoints(
				List(ClickData(100, 1), ClickData(200, 1), ClickData(300, 3), ClickData(400, 4))
			)

			var expectedData = Map(1 -> 4l, 3 -> 2l, 4 -> 2l)
			within(500.millis) {
				aggActor1 ! dataPoints
				expectMsg(AggregationComplete)
				aggActor2 ! dataPoints
				expectMsg(AggregationComplete)
			}

			actorRef ! PersistenceTick
			eventually {
				verify(mockPersistorService, times(1)).persist(expectedData)
			}

			reset(mockPersistorService)
			actorRef ! PersistenceTick
			eventually {
				verify(mockPersistorService, times(1)).persist(expectedData)
			}

			expectedData = Map(1 -> 6l, 3 -> 3l, 4 -> 3l)
			within(500.millis) {
				aggActor3 ! dataPoints
				expectMsg(AggregationComplete)
			}

			reset(mockPersistorService)
			actorRef ! PersistenceTick
			eventually {
				verify(mockPersistorService, times(1)).persist(expectedData)
			}

			aggActor1 ! PoisonPill
			aggActor2 ! PoisonPill
			aggActor3 ! PoisonPill

			actorRef ! PersistenceTick
			eventually {
				verify(mockPersistorService, times(0)).persist(Map.empty)
			}
		}
	}

}
