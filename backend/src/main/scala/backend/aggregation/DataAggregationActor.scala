package backend.aggregation

import akka.actor._
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.{Publish, Subscribe}
import akka.pattern.pipe
import messages.Common._

import scala.collection.mutable
import scala.concurrent.Future
import scala.concurrent.duration._
import scalaz.Scalaz._

/**
	* Doing the calculation
	*/
object DataAggregationActor {
	def props(windowSpan: Int = 24) = Props(classOf[DataAggregationActor], windowSpan)
}
class DataAggregationActor(windowSpan: Int) extends Actor with ActorLogging {
	import context.dispatcher

	val mediator = DistributedPubSub(context.system).mediator
	mediator ! Subscribe("aggregation", self)

	val registerTask = context.system.scheduler.schedule(0.seconds, 10.seconds, mediator,
		Publish("persistor", AggregatorRegistration)
	)
	override def postStop(): Unit = registerTask.cancel()

	var windowIndex = 0 // a strictly increasing index for the "leading edge" window
	def currentWindow = windowIndex % windowSpan // restricts the current window index to a cyclic span

	/**
		* Aggregation data is a Map of Maps where the first map contains window index keys and [[ClickCountMap]] values.
		* When the window index is incremented the [[ClickCountMap]] value is cleared (if non-empty) such that the
		* "leading edge" window can be populated with new aggregates. Upon receiving an [[AggregationDataRequest]], the
		* [[ClickCountMap]] values are reduced and returned to the sender.
		*/
	val aggregationData = mutable.HashMap[Int, mutable.Map[Int, Long]]()

	def receive = {
		case clickData: DataPoints => Future(aggregate(clickData)) pipeTo sender()

		case SlidingWindowTick =>
			windowIndex += 1
			if (aggregationData.contains(currentWindow)) aggregationData(currentWindow).clear

		case AggregationDataRequest => sender() ! AggregationData(reduce())
	}

	protected def aggregate(clickData: DataPoints) = {
		clickData.data.foreach(datum => {
			aggregationData.getOrElseUpdate(currentWindow, new mutable.HashMap().withDefaultValue(0l))(datum.itemId) += 1
		})
		AggregationComplete
	}

	protected def reduce(): Map[Int, Long] = {
		if (aggregationData.nonEmpty) aggregationData.values.map(_.toMap).reduceLeft(_ |+| _) else Map.empty[Int, Long]
	}

}