package backend.aggregation

import akka.actor.{Props, Actor, ActorLogging}
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.Publish
import messages.Common.SlidingWindowTick

import scala.concurrent.duration._

/**
 *
 * @author Adam Martini
 */
object AggregationManager {
	def props(slidingWindowFrequency: FiniteDuration = 1.hour) =
		Props(classOf[AggregationManager], slidingWindowFrequency)
}
class AggregationManager(slidingWindowFrequency: FiniteDuration) extends Actor with ActorLogging {
	import context._

	val mediator = DistributedPubSub(context.system).mediator

	override def preStart() = {
		system.scheduler.scheduleOnce(slidingWindowFrequency, self, SlidingWindowTick)
	}
	override def postRestart(reason: Throwable) = {}  // avoid sending preStart message again

	def receive = {
		case SlidingWindowTick =>
			mediator ! Publish("aggregation", SlidingWindowTick)
			system.scheduler.scheduleOnce(slidingWindowFrequency, self, SlidingWindowTick)
	}

}
