package backend.aggregation

import akka.actor._
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.{Publish, Subscribe}
import backend.service.{CsvPersistorService, PersistorService}
import messages.Common._

import scala.collection.mutable
import scalaz.Scalaz._

/**
	* When prompted by a [[PersistenceTick]], this actor requests and accumulates reduced aggregation data from all
	* [[DataAggregationActor]]s in the cluster, transforms the accumulation to CSV, and persists the data to a file store.
	*/
object AggregationPersistorActor {
	def props(persistorService: PersistorService = CsvPersistorService) =
		Props(classOf[AggregationPersistorActor], persistorService)
}
class AggregationPersistorActor(persistorService: PersistorService) extends Actor with ActorLogging {

	val mediator = DistributedPubSub(context.system).mediator
	mediator ! Subscribe("persistor", self)

	var aggregators = IndexedSeq.empty[ActorRef]
	var aggregatorsQueue = IndexedSeq.empty[ActorRef]

	var persistenceInProgress = false
	var aggregatorAccumulationCount = 0
	var aggregationAccumulator = mutable.HashMap[ActorRef, Map[Int, Long]]()

	def receive = {
		case PersistenceTick if aggregators.nonEmpty =>
			persistenceInProgress = true
			aggregators.foreach(_ ! AggregationDataRequest)

		case AggregationData(data) if persistenceInProgress =>
			// accumulate click count data
			aggregationAccumulator.getOrElseUpdate(sender(), data)

			// persist and clear accumulators if aggregation data has been received from all aggregation actors
			if (aggregationAccumulator.size == aggregators.size) {
				persistorService.persist(aggregationAccumulator.values.reduceLeft(_ |+| _))
				aggregationAccumulator.clear
				persistenceInProgress = false

				// add pending aggregators to official list and clear the queue
				aggregators ++= aggregatorsQueue
				aggregatorsQueue = IndexedSeq.empty[ActorRef]
			}

		case AggregatorRegistration if !aggregators.contains(sender()) =>
			if (persistenceInProgress) {
				aggregatorsQueue = aggregatorsQueue :+ sender()
			} else {
				aggregators = aggregators :+ sender()
			}
			context watch sender()

		case Terminated(a) =>
			aggregators = aggregators.filterNot(_ == a)
			if (persistenceInProgress) {
				aggregationAccumulator -= a
				if (aggregationAccumulator.isEmpty) persistenceInProgress = false
			}
	}

}