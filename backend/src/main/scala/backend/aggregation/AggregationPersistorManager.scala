package backend.aggregation

import akka.actor.{Props, Actor, ActorLogging}
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.Publish
import messages.Common.PersistenceTick

import scala.concurrent.duration._

/**
 *
 * @author Adam Martini
 */
object AggregationPersistorManager {
	def props(persistenceFrequency: FiniteDuration = 15.minutes) =
		Props(classOf[AggregationPersistorManager], persistenceFrequency)
}
class AggregationPersistorManager(persistenceFrequency: FiniteDuration) extends Actor with ActorLogging {
	import context._

	val mediator = DistributedPubSub(context.system).mediator

	override def preStart() = {
		system.scheduler.scheduleOnce(persistenceFrequency, self, PersistenceTick)
	}
	override def postRestart(reason: Throwable) = {}  // avoid sending preStart message again

	def receive = {
		case PersistenceTick =>
			mediator ! Publish("persistor", PersistenceTick)
			system.scheduler.scheduleOnce(persistenceFrequency, self, PersistenceTick)
	}

}
