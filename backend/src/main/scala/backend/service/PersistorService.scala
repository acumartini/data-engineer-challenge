package backend.service

import java.io.{BufferedWriter, File, FileWriter}
import java.text.SimpleDateFormat
import java.util.Date

import messages.Common._

/**
	*
	* @author Adam Martini
	*/
trait PersistorService {
	val location: String
	def filename: String

	def filePath = location + filename

	def persist(data: ClickCountMap)
	protected[service] def transform(data: ClickCountMap): StringBuilder
}

trait CsvPersistorService extends PersistorService
object CsvPersistorService extends CsvPersistorService {

	val location = "./click_data/"
	val dateFormat = new SimpleDateFormat("yyyyMMddhhmm'.csv'")

	def filename = dateFormat.format(new Date())

	def persist(data: ClickCountMap) = {
		val file = new File(filePath)
		val bw = new BufferedWriter(new FileWriter(file))
		bw.write(transform(data).toString)
		bw.close()
	}

	protected[service] def transform(data: ClickCountMap): StringBuilder = {
		val buf = new StringBuilder()
		for { ((id, count), index) <- data.zipWithIndex } {
			if (index != 0) buf += '\n'
			buf ++= s"$id,$count"
		}
		buf
	}

}
