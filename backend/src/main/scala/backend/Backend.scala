package backend

import akka.actor._
import akka.cluster.singleton.{ClusterSingletonManager, ClusterSingletonManagerSettings}
import akka.routing.BalancingPool
import backend.aggregation.{AggregationManager, AggregationPersistorActor, AggregationPersistorManager, DataAggregationActor}
import com.typesafe.config.ConfigFactory

import scala.collection.JavaConversions._
import scala.concurrent.Await
import scala.concurrent.duration._

/**
	* Booting a cluster backend node with actors.
	*/
object Backend extends App {

	val defaultNumAggregators = 5

	// Simple CLI Parsing
	val (port, numAggregators) = args match {
		case Array() => ("0", defaultNumAggregators)
		case Array(portNum) => (portNum, defaultNumAggregators)
		case Array(portNum, aggregatorNum) => (portNum, aggregatorNum.toInt)
		case args => throw new IllegalArgumentException(s"Only accepts a single <port, numAggregators> arguments. Args [$args] are invalid")
	}

	// System Init
	val properties = Map(
		"akka.remote.netty.tcp.port" -> port
	)
	val system = ActorSystem("application", (ConfigFactory parseMap properties)
		.withFallback(ConfigFactory.load())
	)

	// Aggregation
	system.actorOf(BalancingPool(numAggregators).props(DataAggregationActor.props()), "dataAggregationBackend")
	system.actorOf(
		ClusterSingletonManager.props(
			AggregationManager.props(),
			PoisonPill,
			ClusterSingletonManagerSettings(system).withRole("backend")
		),
		"aggregationManager"
	)

	// Persistence
	system.actorOf(
		ClusterSingletonManager.props(
			AggregationPersistorActor.props(),
			PoisonPill,
			ClusterSingletonManagerSettings(system).withRole("backend")
		),
		"aggregationPersistor"
	)
	system.actorOf(
		ClusterSingletonManager.props(
			AggregationPersistorManager.props(),
			PoisonPill,
			ClusterSingletonManagerSettings(system).withRole("backend")
		),
		"aggregationPersistorManager"
	)


	Await.result(system.whenTerminated, Duration.Inf)
}
