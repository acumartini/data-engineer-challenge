package integration

import akka.actor.{Actor, ActorSystem}
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit}
import com.typesafe.config.ConfigFactory
import org.scalatest.concurrent.Eventually
import org.scalatest.mock.MockitoSugar
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

/**
	*
	* @author Adam Martini
	*/
abstract class AbstractIntegrationTest extends TestKit(ActorSystem("IntegrationTest",
	ConfigFactory.parseString(IntegrationTestSpec.config)))
	with DefaultTimeout
	with ImplicitSender
	with WordSpecLike
	with Matchers
	with BeforeAndAfterAll
	with Eventually
	with MockitoSugar

object IntegrationTestSpec {
	val config =
		"""
			|akka {
			|    loglevel = "WARNING"
			|}
		""".stripMargin
}