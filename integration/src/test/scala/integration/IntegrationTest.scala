package integration

import model.ClickData
import org.scalatest.time.{Millis, Seconds, Span}
import play.api.http.Status
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scalaj.http.Http

/**
	*
	* @author Adam Martini
	*/
class IntegrationTest extends AbstractIntegrationTest with Status {

	override implicit val patienceConfig =
		PatienceConfig(timeout = scaled(Span(1, Seconds)), interval = scaled(Span(10, Millis)))

	// Generate data for minimum number of messages (400/s) at maximum size (100 events)
	val r = scala.util.Random
	val clickData = (0 until 1000).map(i => {
		(for (d <- 0 until r.nextInt(100)) yield { ClickData(r.nextLong(), r.nextInt(10000)) }).toList
	}).map(Json.toJson(_).toString)

	Http("http://localhost:9000/data").postData(clickData(0)).header("content-type", "application/json").asString.code

	s"The deployed data aggregation system" should {
		"process at least 400 messages with at most 100 events within 1 second" in {
			var okCount = 0
			var badCount = 0
			clickData.map(data => Future {
				Http("http://localhost:9000/data").postData(data).header("content-type", "application/json").asString.code
			}).foreach(_.onSuccess({
				case OK => okCount += 1
				case _ => badCount += 1
			}))
			eventually {
				badCount shouldEqual 0
				okCount should be > 400
			}
		}
	}

}
