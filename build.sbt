val commonSettings = Seq(
	version := "1.0.0-SNAPSHOT",
	scalaVersion := "2.11.8"
)

//resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

lazy val root = (project in file("."))
	.settings(
		name := "data-engineer-programming-challenge"
	)
	.aggregate(common, frontend, backend, integration)

lazy val common = (project in file("common"))
	.settings(
		name := "cluster-common",
		libraryDependencies ++= (Dependencies.frontend ++ Seq(filters)),
		commonSettings
	)

lazy val backend = (project in file("backend"))
	.enablePlugins(JavaAppPackaging)
	.settings(
		name := "cluster-akka-backend",
		libraryDependencies ++= Dependencies.backend,
		javaOptions ++= Seq("-Xms128m", "-Xmx1024m"),
		fork in run := true,
		commonSettings,
		fork in Test := true
	).dependsOn(common)

lazy val frontend = (project in file("frontend"))
	.enablePlugins(PlayScala, JavaAppPackaging)
	.settings(
		name := "cluster-play-frontend",
		libraryDependencies ++= (Dependencies.frontend ++ Seq(filters, cache)),
		javaOptions ++= Seq("-Xms128m", "-Xmx1024m"),
		fork in run := true,
		commonSettings
	).dependsOn(common)

lazy val integration = (project in file("integration"))
	.enablePlugins(JavaAppPackaging)
	.settings(
		name := "integration-tests",
		libraryDependencies ++= (Dependencies.tests),
		javaOptions ++= Seq("-Xms128m", "-Xmx1024m"),
		fork in run := true,
		commonSettings
	).dependsOn(common)

//// Scala Compiler Options
scalacOptions in ThisBuild ++= Seq(
	"-target:jvm-1.8",
	"-encoding", "UTF-8",
	"-deprecation", // warning and location for usages of deprecated APIs
	"-feature", // warning and location for usages of features that should be imported explicitly
	"-unchecked", // additional warnings where generated code depends on assumptions
	"-Xlint", // recommended additional warnings
	"-Ywarn-adapted-args", // Warn if an argument list is modified to match the receiver
//	"-Ywarn-value-discard", // Warn when non-Unit expression results are unused
	"-Ywarn-inaccessible",
	"-Ywarn-dead-code"
)

